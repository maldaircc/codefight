

def sort_by_height(a):
    numbers = sorted([x for x in a if x != -1])
    count = 0
    for i, n in enumerate(a):
        if n != -1:
            a[i] = numbers[count]
    return a


def sort_by_height_solution1(a):
    people = sorted(x for x in a if x >= 0)
    res = []
    for x in a:
        res.append(x if x < 0 else people.pop(0))
    return res


def sort_by_height_solution2(a):
    v = sorted(filter(lambda x: x != -1, a), reverse=True)
    print(v)
    b = []
    for x in a:
        if x == -1:
            b.append(-1)
        else:
            b.append(v.pop())
    return b

