def all_longest_strings(input_array):
    print input_array
    mayor = len(max(input_array, key=len))
    print max(input_array)
    print mayor
    result = [x for x in input_array if len(x) == mayor]
    # result = filter(lambda x: len(x) == mayor, input_array)

    print result
    return result

lista = ["", "aa", "ad", "vcddddd"]
all_longest_strings(lista)


def all_longest_strings_solution1(input_array):
    print input_array
    mayor = len(input_array[0])
    result = []
    for i in input_array:
        if len(i) == mayor:
            result.append(i)
        if len(i) > mayor:
            result = []
            result.append(i)
            mayor = len(i)

    print result
    return result


def all_longest_strings_solution2(s):
    print [e for e in s if len(e) == len(max(s, key=len))]
    return [e for e in s if len(e) == len(max(s, key=len))]


def all_longest_strings_solution3(input_array):
    m = max(map(len, input_array))
    return filter(lambda x: len(x) == m, input_array)

lista = ["", "aa", "ad", "vcddddd"]
all_longest_strings(lista)
