def matrix_elements_sum(matrix):
    total = 0
    for i in zip(*matrix):
        for j in i:
            if j == 0:
                break
            total += j

    return total

matrix = [[0, 0, 0, 1, 1], [1, 1, 1, 1, 1], [2, 1, 1, 1, 1], [3, 1, 1, 1, 1], [4, 1, 1, 1, 1]]

print matrix_elements_sum(matrix)
