import math
n = 1232


def is_lucky(n):
    d = map(int, str(n))
    l = len(d) / 2
    return sum(d[:l]) == sum(d[:l])


def is_lucky_solution1(n):
    d = map(int, str(n))
    print d
    m = len(d) / 2
    return sum(d[:m]) == sum(d[m:])

is_lucky(n)
