def common_characters_count(s1, s2):
    map1 = {}
    map2 = {}
    answer = 0

    for i in range(len(s1)):
        char = s1[i]
        if char in map1:
            map1[char] += 1
        else:
            map1[char] = 1

    for i in range(len(s2)):
        char = s2[i]
        if char in map2:
            map2[char] += 1
        else:
            map2[char] = 1
    # print ord('a')
    for i in range(ord('a'), ord('z') + 1):
        char = chr(i)
        # print char
        if char in map1 and char in map2:
            answer += min(map1[char], map2[char])
    return answer

s1 = "aabcc"
s2 = "adcaa"
print common_characters_count(s1, s2)
